#!/bin/bash

set -e


[[ -f /var/www/html/index.html ]] && rm -r /var/www/html/*


# Cambio de valores del fichero default.conf 'ServerAlias', 'ServerName' y Serveradmin
if [ -f /etc/apache2/sites-available/000-default.conf ]; then
	rm /etc/apache2/sites-available/000-default.conf
	cp /app/default.conf /etc/apache2/sites-available/default.conf
	a2ensite default.conf
	echo 'ServerName tetris.com' >> /etc/apache2/apache2.conf
	cp /app/tetris.tgz /var/www/html/
	cd /var/www/html && tar -zxvf tetris.tgz
fi



# Inicio de servicios

apachectl -D FOREGROUND

/etc/init.d/apache2 reload

exec "$@"

